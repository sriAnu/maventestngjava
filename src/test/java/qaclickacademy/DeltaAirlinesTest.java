package qaclickacademy;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class DeltaAirlinesTest extends BaseClassTest {
	BaseClassTest baseclass;
	
	@Test
	public void TestOneWayTicket() throws InterruptedException
	{
		//Select one way
		driver.findElement(By.xpath("//span[@aria-owns='selectTripType-desc']/span[@id='selectTripType-val']")).click();
		driver.findElement(By.xpath("//ul[@id ='selectTripType-desc']/li[2]")).click();
						
		//enter FROM city
		driver.findElement(By.id("fromAirportName")).click();
		driver.findElement(By.cssSelector("input#search_input")).sendKeys("LAX");
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("input#search_input")).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.cssSelector("input#search_input")).sendKeys(Keys.ENTER);
		
		//Enter To city
		driver.findElement(By.id("toAirportName")).click();
		driver.findElement(By.cssSelector("input#search_input")).sendKeys("BOS");
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("input#search_input")).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.cssSelector("input#search_input")).sendKeys(Keys.ENTER);
		
		
		//Select departure date
		driver.findElement(By.id("input_departureDate_1")).click();
		LinkedList<WebElement> months = new LinkedList<WebElement>();
		while(!(driver.findElement(By.xpath("//div[@class='calenderContainer'] //span[@class='dl-datepicker-month-0']")).getText().contains("April") || driver.findElement(By.xpath("//div[@class='calenderContainer'] //span[@class='dl-datepicker-month-1']")).getText().contains("April")))
		{
			driver.findElement(By.xpath("//a[@title='To select next month']")).click();
			
		}
		if(driver.findElement(By.xpath("//div[@class='calenderContainer'] //span[@class='dl-datepicker-month-0']")).getText().contains("April"))
		{
			List<WebElement> days = driver.findElements(By.xpath("//div[@class = 'dl-datepicker-group dl-datepicker-group-0'] //td[contains(@class,'day')]"));
			Iterator<WebElement> dayIt= days.iterator();
			while(dayIt.hasNext())
			{
				WebElement day = dayIt.next();
				if (day.getText().equals("11"))
					day.click();
			}
		}
		else
		{
			List<WebElement> days = driver.findElements(By.xpath("//div[@class = 'dl-datepicker-group dl-datepicker-group-1'] //td[contains(@class,'day')]"));
			Iterator<WebElement> dayIt= days.iterator();
			while(dayIt.hasNext())
			{
				WebElement day = dayIt.next();
				if (day.getText().equals("11"))
					day.click();
			}
		}
		
		driver.findElement(By.className("donebutton")).click();
		
		//Select passengers
		
		driver.findElement(By.id("passengers-val")).click();
		//driver.findElement(By.xpath("//span[@class='select-ui-optionList-wrapper'] //li[@data=3]")).click();
		//ui-list-passengers1
		Thread.sleep(2000);
		driver.findElement(By.id("ui-list-passengers3")).click();
		driver.findElement(By.id("btn-book-submit")).click();
			
	}
	
	

}
