package qaclickacademy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseClassTest {
	public static WebDriver driver;

	public BaseClassTest() {
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\chrome\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.delta.com");
	}

}
